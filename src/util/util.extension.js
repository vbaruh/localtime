/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

var util = (function(util){
   util.extension = {
      getExtensionManifest : function() {
         var request = new XMLHttpRequest();
         request.open("GET", chrome.extension.getURL("manifest.json"), false);
         request.send(null);
         
         var manifest = JSON.parse(request.responseText)
         
         return manifest
      }
   }
   
   return util;
}(util || {}))   