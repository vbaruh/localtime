/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

zoneDataHelper=(function(){
   var _zones = null, 
       _rules = null, 
       _regionList = null, 
       _regionToCityMap = null;
       
   var C_NO_REGION = "- No region -";
   
   function _compareCityObject(city1, city2) {
      var result = (city1.cityName < city2.cityName ? -1 : 1) 
      return result
   }
   
   function _getRootZoneObject(zoneName) {
      var zoneObject = _zones[zoneName]
      while ((zoneObject != null) && (typeof zoneObject == "string")) {
         zoneObject = _zones[zoneObject]
      }
      
      return zoneObject   
   }
       
   function _parseData() {
      _regionList = []
      _regionToCityMap = {}
      for(var zoneName in _zones) {
         var zoneObject = _getRootZoneObject(zoneName)
         
         var indexOfSlash = zoneName.indexOf("/")
         var regionName = ""
         var cityName = ""
         if (indexOfSlash == -1) {
            regionName = C_NO_REGION
            cityName = zoneName
         } else {
            regionName = zoneName.substr(0, indexOfSlash)
            cityName = zoneName.substr(indexOfSlash+1)
         }
         
         if (_regionToCityMap[regionName] == null) {
            _regionToCityMap[regionName] = []
         }
         
         // last row of zoneObject is the active one row
         // its first element is the deviation from UTC in minutes
         deviation = -1 * zoneObject[zoneObject.length-1][0]
         
         _regionToCityMap[regionName].push({cityName: cityName, deviation: deviation})
      }

      // sort city objects
      // fill _regionList
      for (var regionName in _regionToCityMap) {
         _regionToCityMap[regionName].sort(_compareCityObject)
         _regionList.push(regionName)
      }
      
      _regionList.sort()
   }
   
   function _joinDicts(dict1, dict2) {
      var newDict = {}
      for (var key in dict1) {
         newDict[key] = dict1[key]
      }
      for (var key in dict2) {
         newDict[key] = dict2[key]
      }
      
      return newDict
   }
   
   function _cityExists(regionName, cityName) {
      if (_regionToCityMap[regionName] == null) {
         return false
      }
      for (var i=0; i < _regionToCityMap[regionName].length; i++) {
         if (_regionToCityMap[regionName][i].cityName == cityName) {
            return true
         }
      }
      
      return false
   }
   
   // public part of the class
   return {
      loadData : function(zoneData) {
         _zones = zoneData.zones;
         _rules = zoneData.rules;
         
         _parseData()
      },
      
      get Zones() {
         return _zones;
      },
      
      get Rules() {
         return _rules;
      },
      
      get RegionList() {
         return _regionList
      },
      
      get RegionToCityMap() {
         return _regionToCityMap;
      },
      
      get NO_REGION_NAME() {
         return C_NO_REGION;
      },
      
      getSubset : function(zoneNames) {
         var newZones = {}
         var newRules = {}
         for (var index in zoneNames) {
            var zoneName = zoneNames[index]
            var zoneObject = _getRootZoneObject(zoneName)
            if (zoneObject != null) {
               newZones[zoneName] = zoneObject
               for (var i=0; i < zoneObject.length; i++) {
                  var ruleSetName = zoneObject[i][1]
                  if (_rules[ruleSetName] != null) {
                     if (newRules[ruleSetName] == null) {
                        newRules[ruleSetName] = _rules[ruleSetName]
                     }
                  }
               }
            }
         }
         
         return {
            zones : newZones,
            rules : newRules
         }         
      },
      
      joinSubsets : function(subset1, subset2) {
         var newZones = _joinDicts(subset1.zones, subset2.zones)
         var newRules = _joinDicts(subset1.rules, subset2.rules)
         
         return {
            zones : newZones,
            rules : newRules
         }
      },

      getZoneName : function(regionName, cityName) {
         if (regionName == "" || cityName == "") {
            return ""
         }
         
         if (regionName == C_NO_REGION) {
            if (_cityExists(C_NO_REGION, cityName)) {
               return cityName
            } else {
               return ""
            }
         } else {
            if (_cityExists(regionName, cityName)) {
               return (regionName + "/" + cityName)
            } else {
               return ""
            }
         }
      },
      
      parseZoneName : function (zoneName) {
         if (zoneName == "") {
            return null
         }
         
         var indexOfSlash = zoneName.indexOf("/")
         var regionName = ""
         var cityName = ""
         if (indexOfSlash == -1) {
            regionName = C_NO_REGION
            cityName = zoneName
         } else {
            regionName = zoneName.substr(0, indexOfSlash)
            cityName = zoneName.substr(indexOfSlash+1)
         }
         
         if (_cityExists(regionName, cityName)) {
            return {
               regionName: regionName,
               cityName: cityName
            }
         } else {
            return null
         }
      }
   }
}())