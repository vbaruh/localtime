/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */
 
zoneSelector = (function(document, zoneDataHelper){
   var _divZoneSelector = null,
       _buttonSelect = null,
       _buttonCancel = null,
       _onselectCallback = null,
       _oncancelCallback = null;

   function _selectButtonClick() {
      var selectedRegion = ""
      var selectedCity = ""
      if (_listRegion.selectedIndex >=0 && _listCity.selectedIndex >=0) {
         selectedRegion = _listRegion.options[_listRegion.selectedIndex].value
         selectedCity = _listCity.options[_listCity.selectedIndex].value
      }
      
      if (_onselectCallback != null) {
         _onselectCallback(selectedRegion, selectedCity)
      }
   }
   
   function _cancelButtonClick() {
      if (_oncancelCallback != null) {
         _oncancelCallback()
      }
   }
   
   function _getHumanReadableDeviation(deviation) {
      var hours = deviation / 60
      var minutes = deviation % 60
      var sign = deviation >= 0 ? "+" : ""
      
      var text = ""
      if (minutes > 0) {
         hours = parseInt(hours)
         text = hours + "." + minutes
      } else {
         text = hours
      }
      
      return (sign + text)
   }
   
   function _onRegionChange() {
      var regionName = _listRegion.options[_listRegion.selectedIndex].value
      
      // clear current cities
      for (var i=_listCity.options.length-1; i >= 0; i--) {
         _listCity.removeChild(_listCity.options[i])
      }
      
      var regionObject = zoneDataHelper.RegionToCityMap[regionName]
      for (var index in regionObject) {
         var cityObject = regionObject[index]
         
         //var sign = cityObject.deviation > 0 ? "+" : ""
         var optionText = cityObject.cityName + " [" + _getHumanReadableDeviation(cityObject.deviation) + "]"
         
         var option = new Option(optionText)
         option.value = cityObject.cityName
         
         _listCity.options[_listCity.options.length] = option
      }
      
      if (_listCity.options.length > 0) {
         _listCity.options[0].selected = true
      }
   }
   
   function _fillListRegion() {
      for(var index in zoneDataHelper.RegionList) {
         _listRegion.options[_listRegion.options.length] = new Option(zoneDataHelper.RegionList[index])
      }
   }   
   
   function initialize() {
      _divZoneSelector = document.getElementById('zoneSelector')
      _divZoneSelector.style.padding = "5px"
      
      _listRegion = document.getElementById('zs_regionList')
      _listRegion.onchange = _onRegionChange;

      _listCity = document.getElementById('zs_cityList')
      
      _buttonSelect = document.getElementById('zs_select')
      _buttonSelect.onclick = _selectButtonClick
      
      _buttonCancel = document.getElementById('zs_cancel')
      _buttonCancel.onclick = _cancelButtonClick
      
      _divZoneSelector.style.backgroundColor = "white"
      _divZoneSelector.style.borderStyle = "solid"
      _divZoneSelector.style.border = "1px"
      
      _fillListRegion()
   }
   
   function _loadHtml() {
      var request = new XMLHttpRequest();
      request.open("GET", "./util/zoneSelector.html", false);
      request.send(null);

      var insertElement = document.createElement('div')
      insertElement.innerHTML = request.responseText
      document.body.appendChild(insertElement)
   }
  
   return {
      init : function() {
         _loadHtml()
         initialize()
      },
      
      //set SelectorCotnrol(){},
      get SelectorControl() {
         return _divZoneSelector;
      },
      
      set onSelectClick(value){
         _onselectCallback = value
      },
      
      get onSelectClick() {
         return _onselectCallback;
      },

      set onCancelClick(value){
         _oncancelCallback = value
      },
      
      get onCancelClick() {
         return _oncancelCallback;
      },
      
      set SelectedZone(zoneName) {
         var regionName = ""
         var cityName = ""
         
         var zoneObj = zoneDataHelper.parseZoneName(zoneName)
         
         if (zoneObj != null) {
            var regionIndex = -1
            for (var i=0; i < _listRegion.options.length; i++) {
               if (_listRegion.options[i].value == zoneObj.regionName) {
                  regionIndex = i
                  break;
               }
            }
            if (regionIndex != -1) {
               _listRegion.options[regionIndex].selected = true
               _onRegionChange()
               var cityIndex = -1;
               for (var i=0; _listCity.options.length; i++) {
                  if (_listCity.options[i].value == zoneObj.cityName) {
                     cityIndex = i
                     break;
                  }
               }
               
               if (cityIndex != -1) {
                  _listCity.options[cityIndex].selected = true
               }
            }
         }
      },
      
      get SelectedZone() {
         var selectedRegion = ""
         var selectedCity = ""
         if (_listRegion.selectedIndex >=0 && _listCity.selectedIndex >=0) {
            selectedRegion = _listRegion.options[_listRegion.selectedIndex].value
            selectedCity = _listCity.options[_listCity.selectedIndex].value
         }
         
         return zoneDataHelper.getZoneName(selectedRegion, selectedCity)
      }
   }
}(document, zoneDataHelper))