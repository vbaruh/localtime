/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

 var util = (function(util, document){
   
   // controls name space
   util.controls = {
      listControl  : {
         addOption : function(listControl, optionValue, optionText) {
            var option = document.createElement("option")
            option.text = optionText
            option.value = optionValue
            listControl.appendChild(option)
            
            return option
         },
         
         getOptionByValue : function(listControl, optionValue) {
            for (var i=0; i < listControl.options.length; i++) {
               if (listControl.options[i].value == optionValue) {
                  return listControl.options[i]
               }
            }
            
            return null            
         },
      
         removeOptionByValue : function(listControl, optionValue) {
            var optionToRemove = this.getOptionByValue(listControl, optionValue)
            if (optionToRemove != null) {
               listControl.removeChild(optionToRemove)
            }
         },
         
         removeAllOptions : function(listControl) {
            for (var i=listControl.options.length-1; i >= 0; i--) {
               listControl.removeChild(listControl.options[i])
            }            
         },
         
         selectOptionByValue : function(listControl, optionValue) {
            var optionToSelect = this.getOptionByValue(listControl, optionValue)
            if (optionToSelect != null) {
               optionToSelect.selected = true
            }
         },
         
         getSelectedOptionValue : function(listControl) {
            if (listControl.selectedIndex == -1) {
               return null
            }
            
            return listControl.options[listControl.selectedIndex].value
         },
         
         swapOptions : function(listControl, index1, index2) {
            var oldValue = listControl.options[index1].value
            var oldText = listControl.options[index1].text
            
            listControl.options[index1].value = listControl.options[index2].value
            listControl.options[index1].text = listControl.options[index2].text
            
            listControl.options[index2].value = oldValue
            listControl.options[index2].text = oldText
         }
      },
      
      
      fileInputControl : {
         promptUserForSingleFile : function(onFileSelectCallback) {
            var id = "_fileInputControl"
            
            var oldControl = document.getElementById(id)
            if (oldControl != null) {
               // removing old control
               oldControl.id = null
               document.body.removeChild(oldControl)
            }
            
            var inputControl = document.createElement('input')
            inputControl.type = "file"
            inputControl.style.display = "none"
            inputControl.id = "_fileInputControl"
            
            inputControl.addEventListener('change', function(event){
               if (typeof onFileSelectCallback == "function") {
                  onFileSelectCallback(event.target.files[0])
               }
            })
            
            document.body.appendChild(inputControl)
            inputControl.click()
         }
      }
   }
   return util;
   
 }(util || {}, document))