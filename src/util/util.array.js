/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */
 
var util = (function(util){

   util.array = {
      arrayAsText : function(theArray) {
         var result = ""
         for (var i=0; i < theArray.length; i++) {
            result = result + theArray[i]
            if (i < theArray.length-1) {
               result = result + "\n"
            }
         }
         return result;         
      },
      
      parseTextToArray : function(value) {
         if (value == null) {
            return []
         }
         
         var arrayValues = value.split("\n")
         var newArray = []
         for (var i=0; i < arrayValues.length; i++) {
            var trimmedValue = arrayValues[i].trim()
            if (trimmedValue != "") {
               newArray.push(trimmedValue)
            }
         }
         
         return newArray
      }
   }
   
   return util;
}(util || {}))   