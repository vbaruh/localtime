/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

 var settings = (function(chrome){
   const S_SYNC_VERSION = "syncVersion"
   const S_CS_COUNT = "csCount"
   const S_LOCAL_TIMEZONE = "localTimezone"
   const S_CONFIG_SET = "configSet"
   const S_TZ_DB = "tzDb"
   const S_EXT_VERSION = "extVersion"
   
   var _manifest = null
   
   function getManifest() {
      if (_manifest == null) {
         _manifest = util.extension.getExtensionManifest()
      }
      
      return _manifest
   }
   
   function getTimezoneSubset(localtimeZone, configSetList) {
      var allZoneNames = []
      
      allZoneNames.push(localtimeZone)
      for (var i in configSetList) {
         allZoneNames.push(configSetList[i].Timezone)
      }
      
      return zoneDataHelper.getSubset(allZoneNames)
   }
   
   function loadJS(jsPath) {
      var request = new XMLHttpRequest();
      request.open("GET", chrome.extension.getURL(jsPath), false);
      request.send(null);
      
      eval(request.responseText)
   }
   
   function loadFullTzDb() {
      // load zonesData from zone.js
      if (typeof zonesData === 'undefined') {
         loadJS("data/zones.js")
      }
      
      if (typeof zoneDataHelper === 'undefined') {
         loadJS("util/zoneDataHelper.js")
         zoneDataHelper.loadData(zonesData)
      }
   }
   
   function refreshLocalTzDb(localTimezone, configSetList, syncVersion) {
      loadFullTzDb()
      
      var result = getTimezoneSubset(localTimezone, configSetList)
      
      var localSettingsObject = {}
      localSettingsObject[S_TZ_DB] = result
      localSettingsObject[S_SYNC_VERSION] = syncVersion
      chrome.storage.local.set(localSettingsObject, function(){})
      
      return result
   }
   
   return {
      load : function(callback) {
         chrome.storage.sync.get(null, function(syncSettingsObject) {
            chrome.storage.local.get(null, function(localSettingsObject){
               
               var configSetList = []
               var localTimezone = ""
               var tzDb = {}
               
               var configSetCount = syncSettingsObject[S_CS_COUNT]
               if (configSetCount == null) {
                  configSetCount = 0
               }
               for (var i=0; i < configSetCount; i++) {
                  var csLabel = S_CONFIG_SET + i
                  configSetList.push(syncSettingsObject[csLabel])
               }
               
               if (syncSettingsObject[S_LOCAL_TIMEZONE] != null) {
                  localTimezone = syncSettingsObject[S_LOCAL_TIMEZONE]
               }
               
               if (localSettingsObject[S_TZ_DB] != null) {
                  tzDb = localSettingsObject[S_TZ_DB]
               } else {
                  // this might happen if the user synchronizes chrome browser on new PC for the firs time
                  tzDb = refreshLocalTzDb(localTimezone, configSetList, syncSettingsObject[S_SYNC_VERSION])
               }
               
               // we should update the local subset of tzdb
               // if the sync storage contains newer version
               // we will do this only if we have initialized information
               var localSyncVersion = localSettingsObject[S_SYNC_VERSION]
               if ((typeof localSyncVersion != 'undefined') && (localSyncVersion < syncSettingsObject[S_SYNC_VERSION])) {
                  tzDb = refreshLocalTzDb(localTimezone, configSetList, syncSettingsObject[S_SYNC_VERSION])
               }
               
               if (typeof callback == "function") {
                  callback(localTimezone, configSetList, tzDb)
               }
            })
         })
      },
      
      save : function(localTimezone, configSetList, callback) {
         chrome.storage.sync.get(S_SYNC_VERSION, function(items) {
            var syncVersion = items[S_SYNC_VERSION]
            if (syncVersion == null) {
               syncVersion = 0
            }
            
            syncVersion += 1
            
            var syncSettingsObject = {}
            syncSettingsObject[S_SYNC_VERSION] = syncVersion
            syncSettingsObject[S_LOCAL_TIMEZONE] = localTimezone
            
            syncSettingsObject[S_CS_COUNT] = configSetList.length
            for (var i=0; i < configSetList.length; i++) {
               var configSetLabel = S_CONFIG_SET + i
               syncSettingsObject[configSetLabel] = configSetList[i]
            }
            
            var localSettingsObject = {}
            localSettingsObject[S_SYNC_VERSION] = syncVersion
            localSettingsObject[S_TZ_DB] = getTimezoneSubset(localTimezone, configSetList)
            localSettingsObject[S_EXT_VERSION] = getManifest().version
            
            chrome.storage.sync.set(syncSettingsObject, function(){
               chrome.storage.local.set(localSettingsObject, function(){
                  if (typeof callback == "function") {
                     callback()
                  }
               })
            })
         })
      },
      
      clear : function() {
         chrome.storage.sync.clear()
         chrome.storage.local.clear()
      },
      
      getSettingsForExport : function(extensionVersion, localTimezone, configSetList) {
         var settings = {
            extensionVersion: extensionVersion,
            localTimezone: localTimezone,
            configSetList : configSetList
         }
         
         return JSON.stringify(settings)
      },
      
      parseImportedSettings : function(importedSettings) {
         try {
            var result = JSON.parse(importedSettings)

            var propertyExist = {
               extensionVersion : false,
               localTimezone : false,
               configSetList : false
            }
            var extraProperty = false
            for ( prop in result ) {
               if (prop == "extensionVersion" || prop == "localTimezone" || prop == "configSetList" ) {
                  propertyExist[prop] = true
               } else {
                  extraProperty = true
                  break
               }
            }
            
            if (extraProperty) {
               return null
            }
            
            if (!propertyExist.extensionVersion || !propertyExist.localTimezone || !propertyExist.configSetList) {
               return null
            }
            
            return result
         } catch(err) {
            return null
         }
      }
   }
}(chrome))