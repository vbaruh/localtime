/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

options = {
   data : [],
   
   // extension manifest
   manifest : null,

   // this variable holds the last selected configuration set in the list control
   prevoiusConfigSetName: "",
   
   // this variable shows is there setting modification or no
   hasBeenModified: false,
   
   // flag that indicates whether the current config set is valid or no
   // calculated by validateConfigSet()
   isCurrentConfigSetValid: false,
   
   
   // controls members
   ConfigSetListControl: null,
   UrlRegexControl: null,
   TimeRegexControl: null,
   ConfigSetTimezoneControl: null,
   OutputFormatControl: null,
   StatusTextControl: null,
   AddConfigSetButton: null,
   RemoveConfigSetButton: null,
   SaveSettingsButton: null,
   CancelChangesButton: null,
   LocalTimezoneControl: null,
   ConfigSetTimezoneSelectButton: null,
   ShowDateMaskSpecifiersButton: null,
   DateMaskTemplatesControl: null,
   AddDateMaskTemplateButton: null,
   ShowOutputSpecifiersButton: null,
   TitleControl: null,
   RenameConfigSetButton: null,
   MoveUpButton: null,
   MoveDownButton: null,
   ConfigSetActiveControl: null,
   ButtonExportSettingsControl: null,
   ButtonImportSettingsControl: null,
   TimeZoneReleaseInfoControl: null,
   CloneConfigSetButton: null,
      
   initControls : function() {
      options.ConfigSetListControl = document.getElementById("configSetList")
      options.UrlRegexControl = document.getElementById("urlRegex")
      options.TimeRegexControl = document.getElementById("tsRegex")
      options.ConfigSetTimezoneControl = document.getElementById("timeZoneInput")
      options.OutputFormatControl = document.getElementById("outputFormatInput")
      options.StatusTextControl = document.getElementById("statusText")
      options.AddConfigSetButton = document.getElementById("buttonAdd")
      options.RemoveConfigSetButton = document.getElementById("buttonRemove")
      options.SaveSettingsButton = document.getElementById("buttonSaveConfigSet")
      options.CancelChangesButton = document.getElementById("buttonCancelConfigSet")
      options.LocalTimezoneControl = document.getElementById("localTimeZone")
      options.LocalTimezoneSelectButton = document.getElementById("buttonSelectLocalTimeZone")
      options.ConfigSetTimezoneSelectButton = document.getElementById("buttonSelectConfigSetTimeZone")   
      options.ShowDateMaskSpecifiersButton = document.getElementById("showMaskSpecifiers")
      options.DateMaskTemplatesControl = document.getElementById("dateMaskTemplates")
      options.AddDateMaskTemplateButton = document.getElementById("addDateMaskTemplate")
      options.ShowOutputSpecifiersButton = document.getElementById("showOutputSpecifiers")
      options.TitleControl = document.getElementById("titleControl")
      options.RenameConfigSetButton = document.getElementById("buttonRename")
      options.MoveUpButton = document.getElementById("buttonUp")
      options.MoveDownButton = document.getElementById("buttonDown")
      options.ConfigSetActiveControl = document.getElementById("configSetActive")
      options.ButtonExportSettingsControl = document.getElementById("buttonExportSettings")
      options.ButtonImportSettingsControl = document.getElementById("importExportSettings")
      options.TimeZoneReleaseInfoControl = document.getElementById("tzdb_releaseInfo")
      options.CloneConfigSetButton = document.getElementById("buttonClone")
   },
   
   assignEventListeners : function() {
      options.AddConfigSetButton.addEventListener('click', options.buttonAddConfigSetClick);
      options.RemoveConfigSetButton.addEventListener('click', options.buttonRemoveConfigSetClick);
      options.SaveSettingsButton.addEventListener('click', options.buttonSaveSettingsClick);
      options.CancelChangesButton.addEventListener('click', options.buttonCancelChangesClick);
      options.ConfigSetListControl.addEventListener('change', options.configSetSelectionChange);
      options.LocalTimezoneSelectButton.addEventListener('click', options.buttonSelectLocalTimezoneClick);
      options.ConfigSetTimezoneSelectButton.addEventListener('click', options.buttonConfigSetTimezoneClick);
      options.ShowDateMaskSpecifiersButton.addEventListener('click', options.showValidSpecifiers);
      options.AddDateMaskTemplateButton.addEventListener('click', options.buttonAddDateMaskTemplateChange);
      options.ShowOutputSpecifiersButton.addEventListener('click', options.showValidSpecifiers);
      options.RenameConfigSetButton.addEventListener('click', options.buttonRenameConfigSet);
      options.MoveUpButton.addEventListener('click', options.buttonMoveUpDownClick);
      options.MoveDownButton.addEventListener('click', options.buttonMoveUpDownClick);
      options.ButtonExportSettingsControl.addEventListener('click', options.buttonExportSettingsClick);
      options.ButtonImportSettingsControl.addEventListener('click', options.buttonImportSettingsClick);
      options.CloneConfigSetButton.addEventListener('click', options.buttonCloneConfigSetClick);
      
      // window unload
      window.addEventListener('beforeunload', options.onOptionsPageUnload)
      
      // assign modification handler where it is applicable
      // if the user type something in these controls, save settings and cancel changes buttons will become enabled
      options.UrlRegexControl.addEventListener('input', options.onModification);
      options.TimeRegexControl.addEventListener('input', options.onModification);
      options.ConfigSetTimezoneControl.addEventListener('input', options.onModification);
      options.OutputFormatControl.addEventListener('input', options.onModification);
      options.LocalTimezoneControl.addEventListener('input', options.onModification);
      options.ConfigSetActiveControl.addEventListener('change', options.onModification);
      
      // assign a handler that performs config set validation
      options.UrlRegexControl.addEventListener('input', options.performValidation);
      options.TimeRegexControl.addEventListener('input', options.performValidation);
      options.ConfigSetTimezoneControl.addEventListener('input', options.performValidation);
      options.OutputFormatControl.addEventListener('input', options.performValidation);
      options.ConfigSetActiveControl.addEventListener('change', options.performValidation);
      options.ConfigSetListControl.addEventListener('change', options.performValidation);
      options.AddDateMaskTemplateButton.addEventListener('click', options.performValidation);
      
      zoneSelector.onCancelClick = options.buttonZoneSelectorCancelClick;
   },
   
   initDefaultDateMaskControl : function() {
      var maskData = [
         "yyyy-mm-dd HH:MM:ss",
         "yyyy-mm-dd HH:MM",
         "yyyy-mm-dd hh:MM tt",
         "dd.mm.yyyy",
         "mm/dd/yyyy",
      ]
      
      util.controls.listControl.removeAllOptions(options.DateMaskTemplatesControl)
      for (var i in maskData) {
         util.controls.listControl.addOption(options.DateMaskTemplatesControl, maskData[i], maskData[i])
      }
   },
   
   initVersionInfo : function() {
      if (options.manifest != null) {
         options.TitleControl.textContent = options.manifest.name + " " + options.manifest.version
      }
   },
   
   initTimezoneDatabaseReleaseInfo : function() {
      // by default display nothing
      options.TimeZoneReleaseInfoControl.textContent = ""
      
      if (zonesData.releaseInfo != null) {
         options.TimeZoneReleaseInfoControl.textContent = "Time zone database release: " + zonesData.releaseInfo
      }
   },
   
   init : function() {
      zoneDataHelper.loadData(zonesData)
      zoneSelector.init()
      
      options.initControls();
      options.assignEventListeners();
      options.loadSettings();
      options.initDefaultDateMaskControl()
      
      options.manifest = util.extension.getExtensionManifest()
      options.initVersionInfo()
      options.initTimezoneDatabaseReleaseInfo()
   },
   
   // event handlers
   buttonAddConfigSetClick : function() {
      var configSetName = window.prompt("Enter name of configuration set. You can use site alias on which this configuration set will operate:")
      if (configSetName == null) {
         return
      }

      var configSet = options.addConfigSet(configSetName)
      if (configSet == null) {
         alert("There is already configuration set with name '" + configSetName + "'.")
         return
      }
      options.setCurrentConfigSet(configSetName)
      
      if (options.data.length == 1) {
         // if this is the first config set, let's add some default values
         options.UrlRegexControl.value = "http://example.com" + "\n" + "https://example.com"
         options.TimeRegexControl.value = "yyyy-mm-dd HH:MM:ss"
      }
   },
   
   buttonRemoveConfigSetClick : function() {
      var selectedName = options.getSelectedConfigSet()
      if (selectedName != null) {
         options.removeConfigSetByName(selectedName)
      }
   },
   
   buttonRenameConfigSet : function() {
      var selectedName = util.controls.listControl.getSelectedOptionValue(options.ConfigSetListControl)
      if (selectedName == null) {
         return
      }
      
      var newName = window.prompt("Enter new name for configuration set '" + selectedName +"':", selectedName)
      if (newName == "" || newName == null || newName == selectedName) {
         return
      }
      var existingConfigSet = options.getConfigSetByName(newName)
      if (existingConfigSet != null) {
         alert("There is another configuration set with name '" + newName + "'.")
         return
      }
      
      if (options.renameConfigSet(selectedName, newName)) {
         options.prevoiusConfigSetName = newName
      }
   },

   configSetSelectionChange : function(event) {
      if (options.prevoiusConfigSetName != "") {
         options.updateConfigSet(options.prevoiusConfigSetName);   
         options.prevoiusConfigSetName = ""
      }
      
      
      var selectedName = options.getSelectedConfigSet()
      if (selectedName != null) {
         options.setCurrentConfigSet(selectedName)
      }
   },

   buttonSaveSettingsClick : function() {
      options.updateConfigSet(options.prevoiusConfigSetName)
      options.storeSettings()
   },

   buttonCancelChangesClick : function() {
      options.loadSettings()
   },

   buttonSelectLocalTimezoneClick : function() {
      zoneSelector.SelectedZone = options.LocalTimezoneControl.value
      popupHelper.popupDiv(zoneSelector.SelectorControl)
      zoneSelector.onSelectClick = function(region, city) {
         popupHelper.hide()
         var selectedZone = zoneDataHelper.getZoneName(region, city)
         if (selectedZone != "") {
            options.LocalTimezoneControl.value = selectedZone
            options.modificationDone(true)
         }
      }
   },
   
   buttonConfigSetTimezoneClick : function() {
      zoneSelector.SelectedZone = options.ConfigSetTimezoneControl.value
      popupHelper.popupDiv(zoneSelector.SelectorControl)
      zoneSelector.onSelectClick = function(region, city) {
         popupHelper.hide()
         var selectedZone = zoneDataHelper.getZoneName(region, city)
         if (selectedZone != "") {
            options.ConfigSetTimezoneControl.value = selectedZone
            options.performValidation()
            options.modificationDone(true)
         }
      }
   },   
    
   buttonZoneSelectorCancelClick : function() {
      popupHelper.hide()
   },
   
   showValidSpecifiers : function(event) {
      var popupDivId = ""
      if (event.toElement.id == options.ShowDateMaskSpecifiersButton.id) {
         popupDivId = "timestampMaskSpecifiersInfo"
      } else if (event.toElement.id == options.ShowOutputSpecifiersButton.id) {
         popupDivId = "outputFormatSpecifiersInfo"
      } else {
         return
      }
      var ctrl = document.getElementById(popupDivId)
      if (ctrl != null) {
         popupHelper.popupDiv(ctrl)
      }
   },
   
   buttonAddDateMaskTemplateChange : function() {
      var value = util.controls.listControl.getSelectedOptionValue(options.DateMaskTemplatesControl)
      if (value != null || value != "") {
         options.TimeRegexControl.value = options.TimeRegexControl.value.trim()
         if (options.TimeRegexControl.value  == "") {
            options.TimeRegexControl.value = value
         } else {
            options.TimeRegexControl.value += "\n" + value
         }
         options.modificationDone(true)
      }
   },
   
   onModification : function() {
      options.modificationDone(true)
   },
   
   onOptionsPageUnload : function() {
      if (options.hasBeenModified) {
         return "There are unsaved settings."
      }
   },
   
   buttonMoveUpDownClick : function(event) {
      if (options.ConfigSetListControl.selectedIndex == -1 ||
         options.data.length <= 1) {
         return
      }
      
      var direction = 0;
      if (event.toElement.id == options.MoveUpButton.id) {
         direction = -1
      } else if (event.toElement.id == options.MoveDownButton.id) {
         direction = 1
      } else {
         return
      }
      
      if (options.ConfigSetListControl.selectedIndex == 0 && direction == -1) {
         return
      }
      
      if (options.ConfigSetListControl.selectedIndex == options.ConfigSetListControl.options.length - 1
         && direction == 1) {
         return
      }
      
      options.moveConfigSet(options.ConfigSetListControl.selectedIndex, direction)
      
      options.modificationDone(true)
   },
   
   buttonExportSettingsClick : function() {
      var jsonSettings = settings.getSettingsForExport(
         options.manifest.version, 
         options.LocalTimezoneControl.value,
         options.data)
         
      util.files.downloadFile(jsonSettings)
   },
   
   buttonImportSettingsClick : function() {
      util.controls.fileInputControl.promptUserForSingleFile(function(file){
         util.files.readTextFile(file, function(content) {
            var settingsObject = settings.parseImportedSettings(content)
            if (settingsObject == null) {
               alert("The specified file is with incorrect format.")
            } else {
               options.importSettings(settingsObject)
            }
         })
      })
   },
   
   buttonCloneConfigSetClick : function() {
      var sourceConfigSetName = options.getSelectedConfigSet()
      var sourceConfigSet = options.getConfigSetByName(sourceConfigSetName)
      if (sourceConfigSet == null) {
         alert("There is no selected config set.")
         return
      }
      
      var configSetName = window.prompt("Enter name of configuration set. You can use site alias on which this configuration set will operate:")
      if (configSetName == null) {
         return
      }
      
      for (var i in options.data) {
         if (options.data[i].Name == configSetName) {
            alert("There is already a configuration set with that name.")
            return
         }
      }
      
      var configSet = new ConfigSet(configSetName, sourceConfigSet)
      util.controls.listControl.addOption(options.ConfigSetListControl, configSet.Name, configSet.Name)
      options.data.push(configSet)
      options.setCurrentConfigSet(configSetName)
      options.modificationDone(true)
   },
   // end of event handlers
   
   setStatusText : function(value) {
      options.StatusTextControl.innerHTML = value
   },
   
   // options page methods
   addConfigSet : function(name) {
      var exist = options.getConfigSetByName(name)
      if (exist != null) {
         return null;
      }

      var configSet = new ConfigSet(name)
      util.controls.listControl.addOption(options.ConfigSetListControl, name, name)
      util.controls.listControl.selectOptionByValue(options.ConfigSetListControl, name)
      options.prevoiusConfigSetName = name

      options.data.push(configSet)
      
      options.modificationDone(true)

      return configSet
   },

   loadConfigSet : function(storageObject) {
      var configSet = new ConfigSet(storageObject)
         
      util.controls.listControl.addOption(options.ConfigSetListControl, configSet.Name, configSet.Name)
      options.data.push(configSet)

      return configSet
   },

   removeConfigSetByName : function(name) {
      var index = options.indexOfConfigSet(name)
      if (index == -1) {
         return false
      }

      var configSetToRemove = options.data[index]

      // remove option element from controls
      util.controls.listControl.removeOptionByValue(options.ConfigSetListControl, name)

      // now remove the object from the array
      options.data.splice(index, 1)
      
      // remove last selected object
      options.prevoiusConfigSetName = ""
         
      if (options.data.length == 0) {
         this.clearConfigSetControls()
      }
      
      options.modificationDone(true)

      return true
   },
   
   renameConfigSet : function (oldName, newName) {
      var configSet = options.getConfigSetByName(oldName)
      var option = util.controls.listControl.getOptionByValue(options.ConfigSetListControl, oldName)
      if (configSet != null && option != null) {
         configSet.Name = newName
         option.value = newName
         option.text = newName
         options.modificationDone(true)
         
         return true
      }
      
      return false
   },
   
   moveConfigSet : function(configSetIndex, direction) {
      var oldConfigSet = options.data[configSetIndex]
      options.data[configSetIndex] = options.data[configSetIndex + direction]
      options.data[configSetIndex + direction] = oldConfigSet
      
      util.controls.listControl.swapOptions(options.ConfigSetListControl, configSetIndex, configSetIndex + direction)
      
      options.setCurrentConfigSet(options.data[configSetIndex + direction].Name)
      return true
   },

   getConfigSetByName : function(name) {
      for(var i=0; i < options.data.length; i++) {
         if (options.data[i].Name == name) {
            return options.data[i]
         }
      }

      return null;
   },

   updateConfigSet : function(name) {
      var configSet = options.getConfigSetByName(name)
      if (configSet == null) {
         return
      }

      configSet.setUrlRegexAsText(options.UrlRegexControl.value)
      configSet.setDateMaskAsText(options.TimeRegexControl.value)
      configSet.OutputFormat = options.OutputFormatControl.value
      configSet.Timezone = options.ConfigSetTimezoneControl.value
      configSet.IsActive = options.ConfigSetActiveControl.checked
      configSet.IsValid = options.isCurrentConfigSetValid
   },

   indexOfConfigSet : function(name) {
      for(var i=0; i < options.data.length; i++) {
         if (options.data[i].Name == name) {
            return i
         }
      }

      return -1;
   },

   setCurrentConfigSet : function(name) {
      var configSet = options.getConfigSetByName(name)
      if (configSet == null) {
         return
      }

      options.UrlRegexControl.value = configSet.getUrlRegexAsText()
      options.TimeRegexControl.value = configSet.getDateMaskAsText()
      options.OutputFormatControl.value = configSet.OutputFormat
      options.ConfigSetTimezoneControl.value = configSet.Timezone
      options.ConfigSetActiveControl.checked = configSet.IsActive
      
      util.controls.listControl.selectOptionByValue(options.ConfigSetListControl, name)
      options.prevoiusConfigSetName = name
   },
   
   getSelectedConfigSet : function() {
      return util.controls.listControl.getSelectedOptionValue(options.ConfigSetListControl)
   },
   
   clearConfigSetControls : function() {
      options.OutputFormatControl.value = ""
      options.ConfigSetTimezoneControl.value = ""
      options.UrlRegexControl.value = ""
      options.TimeRegexControl.value = ""
      
      util.controls.listControl.removeAllOptions(options.ConfigSetListControl)
   },   
   
   modificationDone : function(value) {
      options.hasBeenModified = value
      options.SaveSettingsButton.disabled = !options.hasBeenModified;
      options.CancelChangesButton.disabled = !options.hasBeenModified;
   },
   
   performValidation : function() {
      options.validateConfigSet()
   },
   
   newValidateObject : function(isValid, message) {
      isValid = (typeof isValid === 'undefined' ? false : isValid)
      message = (typeof message === 'undefined' ? "" : message)
      return {
         isValid: isValid,
         message: message
      }
   },
   
   validateUrlRegex : function(value) {
      var result = options.newValidateObject(false, "Please, specify at least one URL.")
      
      result.isValid = (value.trim() != "")
      
      return result
   },
   
   validateTimezone : function(value) {
      var result = options.newValidateObject(false, "The specified zone is not valid. Please, specify valid one.")
      
      result.isValid = (zoneDataHelper.parseZoneName(value) != null)
      
      return result
   },
   
   validateDateMask : function(value) {
      var result = options.newValidateObject(false, "Please, define at least one date mask.")
      
      var dateMaskList = util.array.parseTextToArray(value)
      var notValidIndexes = ""
      if (dateMaskList.length > 0) {
         result.isValid = true
         for (var index in dateMaskList) {
            var dateMatcher = DateParser.getDateParserMatcher(dateMaskList[index])
            if (dateMatcher == null) {
               result.isValid = false
               var rowIndex = parseInt(index) + 1
               notValidIndexes += rowIndex + ","
            }
         }
         
         if (!result.isValid) {
            notValidIndexes = notValidIndexes.substr(0, notValidIndexes.length-1)
            result.message = "Date masks on row(s) " + notValidIndexes + " are not valid."
         }
      }
      
      return result
   },
   
   validateOutputFormat : function(value) {
      var result = options.newValidateObject(false, "Please, specify valid output format.")
      
      var now = new Date()
      try {
         result.isValid = ((value != "") && (now.format(value) != value))
      } catch (ex) {
         result.isValid = false
      }
      
      return result
   },
   
   validateValidateObject : function(validateObject) {
      var result = options.newValidateObject(true, "")
      for (var prop in validateObject) {
         result.isValid = result.isValid && validateObject[prop].isValid
         if (!validateObject[prop].isValid) {
            result.message = result.message + "<br>" + validateObject[prop].message
         }
      }
      
      return result
   },
   
   validateConfigSet : function() {
      var currentConfigName = options.getSelectedConfigSet()
      if (currentConfigName == null) {
         return
      }
      
      var validationObject = {
         url : options.validateUrlRegex(options.UrlRegexControl.value),
         dateMask : options.validateDateMask(options.TimeRegexControl.value),
         timezone : options.validateTimezone(options.ConfigSetTimezoneControl.value),
         outputFormat : options.validateOutputFormat(options.OutputFormatControl.value)
      }

      var finalState = options.validateValidateObject(validationObject)
      options.isCurrentConfigSetValid = finalState.isValid
      if (finalState.isValid) {
         options.setStatusText("<font color=\"green\">Configuration set is valid.</font>")
      } else {
         options.setStatusText("Configuration set is NOT valid.<br><font color=\"red\">" + finalState.message + "</font>")
      }
   },
   
   // settings methods
   storeSettings : function(callback) {
      settings.save(options.LocalTimezoneControl.value, options.data, function(){
         options.modificationDone(false)
         
         if (typeof callback !== "undefined") {
            callback()
         }
      })
   },
   
   loadSettings : function() {
      settings.load(function(localTimezone, configSetList, tzDb){
         options.LocalTimezoneControl.value = localTimezone
         for(var i=0; i < configSetList.length; i++) {
            options.loadConfigSet(configSetList[i])
         }
         
         if (options.data.length > 0) {
            options.setCurrentConfigSet(options.data[0].Name)
            options.performValidation()
         }
         
         options.modificationDone(false)
      })
   },
   
   importSettings : function(settingsObject) {
      var msg = "<font color=\"darkgray\"><pre>Start importing...\n" +
         "</pre></font>"
      options.setStatusText(msg)
         
      options.clearConfigSetControls()
      options.data = []
      
      var validConfigSetCount = 0;
      options.LocalTimezoneControl.value = settingsObject.localTimezone
      for(var i=0; i < settingsObject.configSetList.length; i++) {
         options.loadConfigSet(settingsObject.configSetList[i])
         if (options.data[options.data.length-1].IsValid == true) {
            validConfigSetCount += 1
         }
      }
      

      options.storeSettings(function() {
         if (options.data.length > 0) {
            options.setCurrentConfigSet(options.data[0].Name)
         }

         var msg = "<font color=\"green\"><pre>Import operation completed.\n" +
            "\timported configuration sets: " + options.data.length + "\n" +
            "\tvalid configuration sets: " + validConfigSetCount + "\n" +
            "</pre></font>"
         options.setStatusText(msg)
      })
   }   
} // options   


// initialize options page
document.addEventListener('readystatechange', function () {
   if (document.readyState == "complete") {
      options.init()
   }
});