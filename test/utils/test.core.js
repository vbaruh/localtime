/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

load = function(src) {
   _pendingLoads += 1
   var script = document.createElement("script");
   script.async = false;
   script.type = 'text/javascript';
   script.src = src;
   script.onload = function() { _loaded() }
   document.getElementsByTagName("head")[0].appendChild(script);
}

_pendingLoads = 0

function _loaded(src) {
   _pendingLoads -= 1
   if (_pendingLoads == 0) {
      logInfo("Test execution: started")
      try {
         startTests()
      } catch (err) {
         logError("")
         if (err instanceof AssertError) {
            logError("ASSERTION FAILED -> ", err.message)
         } else { 
            logError("UNHANDLED ERROR -> ", err)
         }
         logError("")
      } finally {
         logInfo("Test execution: completed")
      }
   }
}

function _init() {
   load("./utils/logging.js")
   load("./utils/assert.js")
   load("./utils/string.js")
}

document.addEventListener( "DOMContentLoaded", 
   function(){
      document.removeEventListener( "DOMContentLoaded", arguments.callee, false );
      _init()
   },
   false 
);