/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

String.format = function () {
   var str = arguments[0];
   for (var i=1; i < arguments.length; i++) {
      var pattern = new RegExp("([{]"+(i-1).toString()+"[}])", "g")
      str = str.replace(pattern, arguments[i])
   }
   return str;
};
	
function pad(value, width) {
   if (width == null) {
      width = 2
   }
   
   while (value.length < width) {
      value = "0" + value
   }
   
   return value
}
