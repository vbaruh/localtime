/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */
 
function testZoneConversion(testItem) {
   var sd = testItem.sourceDate
   var ed = testItem.expectedDate
   var dt = new timezoneJS.Date(sd.y, sd.m-1, sd.d, sd.h, sd.mm, sd.ss, testItem.sourceTimezone)
   logInfo("Initial date  : ", dt.toString(), ", ", testItem.sourceTimezone)
   
   dt.setTimezone(testItem.destTimezone)
   logInfo("Converted date: ", dt.toString(), ", ", testItem.destTimezone)
   assertEquals(dt.getYear(), ed.y - 1900)
   assertEquals(dt.getMonth(), ed.m-1)
   assertEquals(dt.getDate(), ed.d)
   assertEquals(dt.getHours(), ed.h)
   assertEquals(dt.getMinutes(), ed.mm)
   assertEquals(dt.getSeconds(), ed.s)
}

function startTests() {
   logInfo("start tests")
   
   var testData = [
      {
         "sourceTimezone" : "Europe/Sofia",
         "destTimezone"   : "Europe/London",
         "sourceDate"     : { y : 2013, m: 6, d: 1, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 6, d: 1, h: 16, mm: 35, s: 00 }
      },
      
      {
         "sourceTimezone" : "Europe/London",
         "destTimezone"   : "Europe/Sofia",
         "sourceDate"     : { y : 2013, m: 6, d: 1, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 6, d: 1, h: 20, mm: 35, s: 00 }
      },
      
      {
         "sourceTimezone" : "Europe/Sofia",
         "destTimezone"   : "US/Pacific",
         "sourceDate"     : { y : 2013, m: 6, d: 1, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 6, d: 1, h: 08, mm: 35, s: 00 }
      },

      {
         "sourceTimezone" : "US/Pacific",
         "destTimezone"   : "Europe/Sofia",
         "sourceDate"     : { y : 2013, m: 6, d: 1, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 6, d: 2, h: 04, mm: 35, s: 00 }
      },
      
      {
         "sourceTimezone" : "Europe/London",
         "destTimezone"   : "US/Pacific",
         "sourceDate"     : { y : 2013, m: 6, d: 1, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 6, d: 1, h: 10, mm: 35, s: 00 }
      },
            
      {
         // 9 hours diff during march - there is short period
         // during which the time diff is 9 hours, instead of 10
         // US Rule - March, Sunday >= 8
         // EU Rule - March, Last Sunday
         
         "sourceTimezone" : "Europe/Sofia",
         "destTimezone"   : "US/Pacific",
         "sourceDate"     : { y : 2013, m: 3, d: 11, h: 18, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 3, d: 11, h: 09, mm: 35, s: 00 }
      },
      
      {
         // 9 hours diff during october - there is short period
         // during which the time diff is 9 hours, instead of 10
         // US Rule - November, Sunday >= 1
         // EU Rule - October, Last Sunday
         //
         "sourceTimezone" : "Europe/Sofia",
         "destTimezone"   : "US/Pacific",
         "sourceDate"     : { y : 2013, m: 10, d: 28, h: 19, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 10, d: 28, h: 10, mm: 35, s: 00 }
      },
      
      {
         // 10 hours diff after US push back one hour
         "sourceTimezone" : "Europe/Sofia",
         "destTimezone"   : "US/Pacific",
         "sourceDate"     : { y : 2013, m: 11, d: 04, h: 19, mm: 35, s: 00 },
         "expectedDate"   : { y : 2013, m: 11, d: 04, h: 09, mm: 35, s: 00 }
      }           
                  
   ]
   
   timezoneJS.timezone.loadPreparsedData(zonesData)
   
   for ( var i=0; i < testData.length; i++) {
      testZoneConversion(testData[i])
      logInfo("----------------------")
   }
   
   logInfo("end tests")
}