/*
 * This file is part of LocalTime https://bitbucket.org/vbaruh/localtime/
 * and it is licensed under MIT license. Check MIT-LICENSE.txt for details
 */

function dateCreate(year, month, day, hours, minutes, seconds, milliseconds, originalDate) {
   lastDate = new Date(year, month-1, day, hours, minutes, seconds, milliseconds)
   return ""
}

function testDateParser(mask, dateList) {
   var dateMatcher = DateParser.getDateParserMatcher(mask)
   var msg = String.format("testing mask : '{0}', date : '{1}', regex: '{2}'", mask, dateList[0].format(mask), dateMatcher.regex.toString())
   logInfo(msg)
   logIndentIncrease()
   try {
      for(var i=0; i < dateList.length; i++) {
         var dateStr = dateList[i].format(mask)
         lastDate = null
         logTrace("date       : ", dateStr)

         DateParser.matchDate(dateStr, dateMatcher, dateCreate)

         var lastDateStr = lastDate != null ? lastDate.format(mask) : ""
         logTrace("lastDateStr: ", lastDateStr)

         var assertMessage = String.format("expectedDate: '{0}', actualDate: '{1}'", dateStr, lastDateStr)
         assert( (lastDateStr == dateStr) , assertMessage)
      }
   } finally {
      logIndentDecrease()
   }
}


function multipleTests() {
   var dtArray = [
      new Date(2016,00,01,17,22,00),
      new Date(2016,00,31,17,22,00),

      new Date(2016,01,1,17,22,00),
      new Date(2016,01,29,17,22,00),

      new Date(2016,02,1,17,22,00),
      new Date(2016,02,31,17,22,00),

      new Date(2016,03,1,17,22,00),
      new Date(2016,03,30,17,22,00),

      new Date(2016,04,1,17,22,00),
      new Date(2016,04,31,17,22,00),

      new Date(2016,05,1,17,22,00),
      new Date(2016,05,30,17,22,00),

      new Date(2016,06,1,17,22,00),
      new Date(2016,06,31,17,22,00),

      new Date(2016,07,1,17,22,00),
      new Date(2016,07,31,17,22,00),

      new Date(2016,08,1,17,22,00),
      new Date(2016,08,30,17,22,00),

      new Date(2016,09,1,17,22,00),
      new Date(2016,09,31,17,22,00),

      new Date(2016,10,1,17,22,00),
      new Date(2016,10,30,17,22,00),

      new Date(2016,11,1,17,22,00),
      new Date(2016,11,31,17,22,00),

      new Date(2016,11,1,  0,0,00),
      new Date(2016,11,1,  0,9,00),
      new Date(2016,11,1,  0,10,00),
      new Date(2016,11,1,  0,20,00),
      new Date(2016,11,1,  0,30,00),
      new Date(2016,11,1,  0,40,00),
      new Date(2016,11,1,  0,50,00),
      new Date(2016,11,1,  0,59,00),

      new Date(2016,11,1,  1,0,00),
      new Date(2016,11,1,  1,59,00),

      new Date(2016,11,1,  9,0,00),
      new Date(2016,11,1,  9,59,00),

      new Date(2016,11,1,  10,00,01),
      new Date(2016,11,1,  11,00,01),
      new Date(2016,11,1,  12,00,01),
      new Date(2016,11,1,  13,00,01),

      new Date(2016,11,1,  23,0,00),
      new Date(2016,11,1,  23,59,00),

      new Date(2016,11,1,  23,00,01),
      new Date(2016,11,1,  23,59,59),

      new Date(2016,11,1,  23,00,01,0),
      new Date(2016,11,1,  23,00,01,1),
      new Date(2016,11,1,  23,00,01,99),
      new Date(2016,11,1,  23,00,01,100),
      new Date(2016,11,1,  23,00,01,999),
   ]

   maskData = [
      "yyyy-dd-mm hh:MMtt",
      "yyyy-mm-dd hh:MMtt",

      "dd-mm-yyyy",
      "dd/mm/yyyy",

      "mm/dd/yyyy",
      "mm/dd/yy",

      "mm.dd.yyyy",
      "mm.dd.yy",

      "yyyy-mm-dd'T'HH:MM:ss",
      "yyyy-mm-dd'T'H:MM:ss",
      "yyyy-mm-dd'T'H:M:s",
      "yy-mm-dd'T'H:M:s",
      "yy-m-dd'T'H:M:s",
      "yy-m-d'T'H:M:s",

      "h:M:st",
      "h:M:stt",

      "hh:MM:ss tt",
      "hh:M:ss tt",
      "hh:MMtt",

      "ddd mmm dd, yyyy",
      "dddd mmm dd, yyyy",
      "dddd mmmm dd, yyyy",
      
      "yyyy-mm-dd HH:MM:ss,l"
   ]

   for (var i in maskData) {
      testDateParser(maskData[i], dtArray)
   }
}

function testHourPeriodsWithDots() {
   testData = [
      {
         info : "testing a.m. and p.m. day periods",
         
         dateStrList : [
            { dateStr: "2012-12-12 05:12 a.m.", expected : "2012-12-12 05:12 am"},
            { dateStr: "2012-12-12 05:12 A.M.", expected : "2012-12-12 05:12 am"},
            { dateStr: "2012-12-12 05:12 P.M.", expected : "2012-12-12 05:12 pm"},
            { dateStr: "2012-12-12 05:12 p.m.", expected : "2012-12-12 05:12 pm"}
         ],
         dateMask : "yyyy-mm-dd hh:MM tt",
         expectDateMask : "yyyy-mm-dd hh:MM tt"
      }
   ]
   testTestData(testData)
}

function testOrdinalDays() {
   testData = [
      {
         info : "testing ordinal days with single day placeholder",
         dateStrList : [
            { dateStr : "Mar 1st 2012", expected : "2012-03-01" },
            { dateStr : "Mar 2nd 2012", expected : "2012-03-02" },
            { dateStr : "Mar 3rd 2012", expected : "2012-03-03" },
            { dateStr : "Mar 4th 2012", expected : "2012-03-04" },
            { dateStr : "Mar 5th 2012", expected : "2012-03-05" },
            { dateStr : "Mar 6th 2012", expected : "2012-03-06" },
            { dateStr : "Mar 7th 2012", expected : "2012-03-07" },
            { dateStr : "Mar 8th 2012", expected : "2012-03-08" },
            { dateStr : "Mar 9th 2012", expected : "2012-03-09" },
            { dateStr : "Mar 10th 2012", expected : "2012-03-10" },

            { dateStr : "Mar 11th 2012", expected : "2012-03-11" },
            { dateStr : "Mar 15th 2012", expected : "2012-03-15" },
            { dateStr : "Mar 19th 2012", expected : "2012-03-19" },

            { dateStr : "Mar 21st 2012", expected : "2012-03-21" },
            { dateStr : "Mar 22nd 2012", expected : "2012-03-22" },
            { dateStr : "Mar 22d 2012", expected : "2012-03-22" },
            { dateStr : "Mar 23rd 2012", expected : "2012-03-23" },
            { dateStr : "Mar 24th 2012", expected : "2012-03-24" },

            { dateStr : "Mar 30th 2012", expected : "2012-03-30" },
            { dateStr : "Mar 31st 2012", expected : "2012-03-31" }
         ],
         dateMask : "mmm d yyyy",
         expectDateMask: "yyyy-mm-dd"
      },
      {
         info : "testing ordinal days with double day placeholder",
         dateStrList : [
            { dateStr : "Mar 01st 2012", expected : "2012-03-01" },
            { dateStr : "Mar 02nd 2012", expected : "2012-03-02" },

            { dateStr : "Mar 03rd 2012", expected : "2012-03-03" },
            { dateStr : "Mar 04th 2012", expected : "2012-03-04" },

            { dateStr : "Mar 11th 2012", expected : "2012-03-11" },
            { dateStr : "Mar 15th 2012", expected : "2012-03-15" },
            { dateStr : "Mar 19th 2012", expected : "2012-03-19" },

            { dateStr : "Mar 21st 2012", expected : "2012-03-21" },
            { dateStr : "Mar 22nd 2012", expected : "2012-03-22" },
            { dateStr : "Mar 22d 2012", expected : "2012-03-22" },
            { dateStr : "Mar 23rd 2012", expected : "2012-03-23" },
            { dateStr : "Mar 24th 2012", expected : "2012-03-24" },

            { dateStr : "Mar 31st 2012", expected : "2012-03-31" }

         ],

         dateMask : "mmm dd yyyy",
         expectDateMask: "yyyy-mm-dd"
      }
   ]
   testTestData(testData)
}

function testMilliseconds() {
   testData = [
      {
         info : "parsing milliseconds, not padded",
         dateStrList : [
            { dateStr : "2012-12-12 18:55:32,0", expected : "2012-12-12 18:55:32,000" },
            { dateStr : "2012-12-12 18:55:32,9", expected : "2012-12-12 18:55:32,009" },
            
            { dateStr : "2012-12-12 18:55:32,10", expected : "2012-12-12 18:55:32,010" },
            { dateStr : "2012-12-12 18:55:32,99", expected : "2012-12-12 18:55:32,099" },
            
            { dateStr : "2012-12-12 18:55:32,100", expected : "2012-12-12 18:55:32,100" },
            { dateStr : "2012-12-12 18:55:32,999", expected : "2012-12-12 18:55:32,999" }
         ],

         dateMask : "yyyy-mm-dd HH:MM:ss,l",
         expectDateMask: "yyyy-mm-dd HH:MM:ss,l"
      },
      
      {
         info : "parsing milliseconds, padded to 3 digits",
         dateStrList : [
            { dateStr : "2012-12-12 18:55:32,000", expected : "2012-12-12 18:55:32,000" },
            { dateStr : "2012-12-12 18:55:32,009", expected : "2012-12-12 18:55:32,009" },
            
            { dateStr : "2012-12-12 18:55:32,010", expected : "2012-12-12 18:55:32,010" },
            { dateStr : "2012-12-12 18:55:32,099", expected : "2012-12-12 18:55:32,099" },
            
            { dateStr : "2012-12-12 18:55:32,100", expected : "2012-12-12 18:55:32,100" },
            { dateStr : "2012-12-12 18:55:32,999", expected : "2012-12-12 18:55:32,999" }
         ],

         dateMask : "yyyy-mm-dd HH:MM:ss,lll",
         expectDateMask: "yyyy-mm-dd HH:MM:ss,l"
      },
      
      {
         info : "parsing milliseconds, padded to 4 digits",
         dateStrList : [
            { dateStr : "2012-12-12 18:55:32,0000", expected : "2012-12-12 18:55:32,0000" },
            { dateStr : "2012-12-12 18:55:32,0009", expected : "2012-12-12 18:55:32,0009" },
            
            { dateStr : "2012-12-12 18:55:32,0010", expected : "2012-12-12 18:55:32,0010" },
            { dateStr : "2012-12-12 18:55:32,0099", expected : "2012-12-12 18:55:32,0099" },
            
            { dateStr : "2012-12-12 18:55:32,0100", expected : "2012-12-12 18:55:32,0100" },
            { dateStr : "2012-12-12 18:55:32,0999", expected : "2012-12-12 18:55:32,0999" }
         ],

         dateMask : "yyyy-mm-dd HH:MM:ss,llll",
         expectDateMask: "yyyy-mm-dd HH:MM:ss,'0'l"
      }      
   ]
   
   testTestData(testData)
}

function testNbspParsing() {
   testData = [
      {
         info : "test parsing of string which contains &nbsp; sequence",
         
         dateStrList : [
            { dateStr: "2012&nbsp;12 12&nbsp;05:12&nbsp;a.m._ KKK _", expected : "2012 12 12 05:12 am_ KKK _"}
         ],
         dateMask : "yyyy mm dd hh:MM tt'_ KKK _'",
         expectDateMask : "yyyy mm dd hh:MM tt_ KKK _"
      }
   ]
   testTestData(testData)
}

function testTestData(testData) {
   for ( var i in testData ) {
      var testItem = testData[i]
      logInfo ("testData : ", testItem.info, ", date mask '", testItem.dateMask, "'")
      try {
         logIndentIncrease()
         var dateMatcher = DateParser.getDateParserMatcher(testItem.dateMask)
         for ( var j in testItem.dateStrList) {
            lastDate = null
            var testCouple = testItem.dateStrList[j]
            DateParser.matchDate(testCouple.dateStr, dateMatcher, dateCreate)

            var lastDateStr = lastDate != null ? lastDate.format(testItem.expectDateMask) : ""
            logTrace("parsed ", lastDateStr, ", from ", testCouple.dateStr)

            assert( (lastDate != null ), "last parsing failed, lastDate is null")
            
            var lastDateStr = lastDate.format(testItem.expectDateMask)
            var expectedDateStr = testCouple.expected
            var assertMessage = String.format("expectedDate: '{0}', actualDate: '{1}'", expectedDateStr, lastDateStr)
            assert( (expectedDateStr == lastDateStr) , assertMessage)
         }
      } finally {
         logIndentDecrease()
      }
   }
}

function startTests() {
   logLevel = 3
   multipleTests()
   logInfo("-------------------------------------------")
   testHourPeriodsWithDots()
   testOrdinalDays()
   testMilliseconds()
   testNbspParsing()
}